#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define epoca 2000
#define K 0.5f

//Funcion de Entrenamiento Perceptron
float EntNt(float, float, float);
//Funcion para las salidas
float InitNt(float, float);
//Sigmoide
float sigmoide_AND(float);
float sigmoide_OR(float);
float sigmoide_XOR(float);
//pesos aleatorios
void pesos_initNt_AND();
void pesos_initNt_OR();
void pesos_initNt_XOR();

float Pesos_AND[5];
float Pesos_OR[5];
float Pesos_XOR[5];
float bias_AND=0.5f;
float bias_OR=0.5f;
float bias_XOR=0.5f;


float EntNt_AND( float x0, float x1, float x2, float x3, float x4, float target ){
  float net = 0;
  float out = 0;
  float delta[5];  //Es la variacion de los pesos sinapticos
  float Error;

  net = Pesos_AND[0]*x0 + Pesos_AND[1]*x1 + Pesos_AND[2]*x2 + Pesos_AND[3]*x3 +Pesos_AND[4]*x4 -bias_AND;
  net = sigmoide_AND( net );

  Error = target - net;

  bias_AND -= K*Error;  //Como el bias es siempre 1, pongo que
                    //el bias incluye ya su peso sinaptico

  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde
  delta[1] = K*Error * x1;
  delta[2] = K*Error * x2;
  delta[3] = K*Error * x3;
  delta[4] = K*Error * x2;  //al error cometido, por la entrada correspondiente

  Pesos_AND[0] += delta[0];  //Se ajustan los nuevos valores
  Pesos_AND[1] += delta[1];
  Pesos_AND[2] += delta[2];
  Pesos_AND[3] += delta[3];
  Pesos_AND[4] += delta[4];  //de los pesos sinapticos

  out=net;
  return out;
}

float EntNt_OR( float x0, float x1, float x2, float x3, float x4, float target  ){
  float net = 0;
  float out = 0;
  float delta[5];  //Es la variacion de los pesos sinapticos
  float Error;

  net = Pesos_OR[0]*x0 + Pesos_OR[1]*x1 + Pesos_OR[2]*x2 + Pesos_OR[3]*x3 + Pesos_OR[4]*x4 -bias_OR;
  net = sigmoide_OR( net );

  Error = target - net;

  bias_OR -= K*Error;  //Como el bias es siempre 1, pongo que
                    //el bias incluye ya su peso sinaptico

  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde
  delta[1] = K*Error * x1;
  delta[2] = K*Error * x2;
  delta[3] = K*Error * x3;
  delta[4] = K*Error * x4;  //al error cometido, por la entrada correspondiente

  Pesos_OR[0] += delta[0];  //Se ajustan los nuevos valores
  Pesos_OR[1] += delta[1];  //de los pesos sinapticos
  Pesos_OR[2] += delta[2];
  Pesos_OR[3] += delta[3];
  Pesos_OR[4] += delta[4];

  out=net;
  return out;
}

float EntNt_XOR( float x0, float x1, float x2, float x3, float x4, float target ){
  float net = 0;
  float out = 0;
  float delta[5];  //Es la variacion de los pesos sinapticos
  float Error;

  net = Pesos_XOR[0]*x0 + Pesos_XOR[1]*x1 + Pesos_XOR[2]*x2 + Pesos_XOR[3]*x3 + Pesos_XOR[4]*x4 -bias_XOR;
  net = sigmoide_XOR( net );

  Error = target - net;

  bias_XOR -= K*Error;  		//Como el bias es siempre 1, pongo que
							//el bias incluye ya su peso sinaptico

  delta[0] = K*Error * x0;  //la variacion de los pesos sinapticos corresponde
  delta[1] = K*Error * x1;  //al error cometido, por la entrada correspondiente
  delta[2] = K*Error * x2;
  delta[3] = K*Error * x3;
  delta[4] = K*Error * x4;

  Pesos_XOR[0] += delta[0];  //Se ajustan los nuevos valores
  Pesos_XOR[1] += delta[1];  //de los pesos sinapticos
  Pesos_XOR[2] += delta[2];
  Pesos_XOR[3] += delta[3];
  Pesos_XOR[4] += delta[4];

  out=net;
  return out;
}

float InitNt_AND( float x0, float x1, float x2, float x3, float x4  ){
  float net = 0;
  float out = 0;

  net = Pesos_AND[0]*x0 + Pesos_AND[1]*x1 + Pesos_AND[2]*x2 + Pesos_AND[3]*x3 +Pesos_AND[4]*x4 -bias_AND;
  net=sigmoide_AND( net );

  out=net;
  return out;
}

float InitNt_OR( float x0, float x1, float x2, float x3, float x4 ){
  float net = 0;
  float out = 0;

  net = Pesos_AND[0]*x0 + Pesos_AND[1]*x1 + Pesos_AND[2]*x2 + Pesos_AND[3]*x3 + Pesos_AND[4]*x4 -bias_AND;
  net=sigmoide_AND( net );

  out=net;
  return out;
}

float InitNt_XOR( float x0, float x1, float x2, float x3, float x4 ){
  float net = 0;
  float out = 0;

  net = Pesos_XOR[0]*x0 + Pesos_XOR[1]*x1 + Pesos_XOR[2]*x2 + Pesos_XOR[3]*x3 + Pesos_XOR[3]*x4 -bias_XOR;
  net=sigmoide_XOR( net );

  out=net;
  return out;
}

void pesos_initNt_AND(){
  int i;
  for(  i = 0; i < 5; i++ )
  {
    Pesos_AND[i] = (float)rand()/RAND_MAX;
  }
}

void pesos_initNt_OR(){
  int i;
  for(  i = 0; i < 5; i++ )
  {
    Pesos_OR[i] = (float)rand()/RAND_MAX;
  }
}

void pesos_initNt_XOR(){
  int i;
  for(  i = 0; i < 5; i++ )
  {
    Pesos_XOR[i] = (float)rand()/RAND_MAX;
  }
}

float sigmoide_AND( float s ){
  return (1/(1+ exp(-1*s)));
}

float sigmoide_OR( float s ){
  return (1/(1+ exp(-1*s)));
}

float sigmoide_XOR( float s ){
  return (1/(1+ exp(-1*s)));
}

int main(){
  int i=0;
  float apr;
  pesos_initNt_OR();
  while(i<epoca){
    i++;
    //------------------------------------------------------------------
    printf("Salida Entrenamiento OR\n");
    apr=EntNt_OR(1,1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=EntNt_OR(1,1,1,1,0,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=EntNt_OR(1,1,1,0,1,0);
    printf("1,1,1,0,1=%f\n",apr);
    apr=EntNt_OR(1,1,1,0,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=EntNt_OR(1,1,0,1,1,0);
    printf("1,1,0,1,1=%f\n",apr);
    apr=EntNt_OR(1,1,0,1,0,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=EntNt_OR(1,1,0,0,1,0);
    printf("1,1,0,0,1=%f\n",apr);
    apr=EntNt_OR(1,1,0,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=EntNt_OR(1,0,1,1,1,0);
    printf("1,0,1,1,1=%f\n",apr);
    apr=EntNt_OR(1,0,1,1,0,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=EntNt_OR(1,0,1,0,1,0);
    printf("1,0,1,0,1=%f\n",apr);
    apr=EntNt_OR(1,0,1,0,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=EntNt_OR(1,0,0,1,1,0);
    printf("1,0,0,1,1=%f\n",apr);
    apr=EntNt_OR(1,0,0,1,0,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=EntNt_OR(1,0,0,0,1,0);
    printf("1,0,0,0,1=%f\n",apr);
    apr=EntNt_OR(1,0,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=EntNt_OR(0,1,1,1,1,0);
    printf("0,1,1,1,1=%f\n",apr);
    apr=EntNt_OR(0,1,1,1,0,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=EntNt_OR(0,1,1,0,1,0);
    printf("0,1,1,0,1=%f\n",apr);
    apr=EntNt_OR(0,1,1,0,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=EntNt_OR(0,1,0,1,1,0);
    printf("0,1,0,1,1=%f\n",apr);
    apr=EntNt_OR(0,1,0,1,0,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=EntNt_OR(0,1,0,0,1,0);
    printf("0,1,0,0,1=%f\n",apr);
    apr=EntNt_OR(0,1,0,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=EntNt_OR(0,0,1,1,1,0);
    printf("0,0,1,1,1=%f\n",apr);
    apr=EntNt_OR(0,0,1,1,0,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=EntNt_OR(0,0,1,0,1,0);
    printf("0,0,1,0,1=%f\n",apr);
    apr=EntNt_OR(0,0,1,0,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=EntNt_OR(0,0,0,1,1,0);
    printf("0,0,0,1,1=%f\n",apr);
    apr=EntNt_OR(0,0,0,1,0,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=EntNt_OR(0,0,0,0,1,0);
    printf("0,0,0,0,1=%f\n",apr);
    apr=EntNt_OR(0,0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);

    printf("\n");
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", Pesos_OR[0]);
    printf("Peso 2 = %f\n", Pesos_OR[1]);
    printf("Peso 3 = %f\n", Pesos_OR[2]);
    printf("Peso 4 = %f\n", Pesos_OR[3]);
    printf("Peso 5 = %f\n", Pesos_OR[4]);
    printf("Bias");
    printf("Bias = %f",bias_OR);
    printf("Resultados\n");
    apr=InitNt_OR(1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=InitNt_OR(1,1,1,1,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=InitNt_OR(1,1,1,0,1);
    printf("1,1,1,0,1=%f\n",apr);
    apr=InitNt_OR(1,1,1,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=InitNt_OR(1,1,0,1,1);
    printf("1,1,0,1,1=%f\n",apr);
    apr=InitNt_OR(1,1,0,1,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=InitNt_OR(1,1,0,0,1);
    printf("1,1,0,0,1=%f\n",apr);
    apr=InitNt_OR(1,1,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=InitNt_OR(1,0,1,1,1);
    printf("1,0,1,1,1=%f\n",apr);
    apr=InitNt_OR(1,0,1,1,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=InitNt_OR(1,0,1,0,1);
    printf("1,0,1,0,1=%f\n",apr);
    apr=InitNt_OR(1,0,1,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=InitNt_OR(1,0,0,1,1);
    printf("1,0,0,1,1=%f\n",apr);
    apr=InitNt_OR(1,0,0,1,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=InitNt_OR(1,0,0,0,1);
    printf("1,0,0,0,1=%f\n",apr);
    apr=InitNt_OR(1,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=InitNt_OR(0,1,1,1,1);
    printf("0,1,1,1,1=%f\n",apr);
    apr=InitNt_OR(0,1,1,1,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=InitNt_OR(0,1,1,0,1);
    printf("0,1,1,0,1=%f\n",apr);
    apr=InitNt_OR(0,1,1,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=InitNt_OR(0,1,0,1,1);
    printf("0,1,0,1,1=%f\n",apr);
    apr=InitNt_OR(0,1,0,1,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=InitNt_OR(0,1,0,0,1);
    printf("0,1,0,0,1=%f\n",apr);
    apr=InitNt_OR(0,1,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=InitNt_OR(0,0,1,1,1);
    printf("0,0,1,1,1=%f\n",apr);
    apr=InitNt_OR(0,0,1,1,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=InitNt_OR(0,0,1,0,1);
    printf("0,0,1,0,1=%f\n",apr);
    apr=InitNt_OR(0,0,1,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=InitNt_OR(0,0,0,1,1);
    printf("0,0,0,1,1=%f\n",apr);
    apr=InitNt_OR(0,0,0,1,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=InitNt_OR(0,0,0,0,1);
    printf("0,0,0,0,1=%f\n",apr);
    apr=InitNt_OR(0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);

    //-----------------------------------------------------------------------
    printf("Salida Entrenamiento AND\n");
    apr=EntNt_AND(1,1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=EntNt_AND(1,1,1,1,0,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=EntNt_AND(1,1,1,0,1,0);
    printf("1,1,1,0,1=%f\n",apr);
    apr=EntNt_AND(1,1,1,0,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=EntNt_AND(1,1,0,1,1,0);
    printf("1,1,0,1,1=%f\n",apr);
    apr=EntNt_AND(1,1,0,1,0,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=EntNt_AND(1,1,0,0,1,0);
    printf("1,1,0,0,1=%f\n",apr);
    apr=EntNt_AND(1,1,0,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=EntNt_AND(1,0,1,1,1,0);
    printf("1,0,1,1,1=%f\n",apr);
    apr=EntNt_AND(1,0,1,1,0,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=EntNt_AND(1,0,1,0,1,0);
    printf("1,0,1,0,1=%f\n",apr);
    apr=EntNt_AND(1,0,1,0,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=EntNt_AND(1,0,0,1,1,0);
    printf("1,0,0,1,1=%f\n",apr);
    apr=EntNt_AND(1,0,0,1,0,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=EntNt_AND(1,0,0,0,1,0);
    printf("1,0,0,0,1=%f\n",apr);
    apr=EntNt_AND(1,0,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=EntNt_AND(0,1,1,1,1,0);
    printf("0,1,1,1,1=%f\n",apr);
    apr=EntNt_AND(0,1,1,1,0,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=EntNt_AND(0,1,1,0,1,0);
    printf("0,1,1,0,1=%f\n",apr);
    apr=EntNt_AND(0,1,1,0,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=EntNt_AND(0,1,0,1,1,0);
    printf("0,1,0,1,1=%f\n",apr);
    apr=EntNt_AND(0,1,0,1,0,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=EntNt_AND(0,1,0,0,1,0);
    printf("0,1,0,0,1=%f\n",apr);
    apr=EntNt_AND(0,1,0,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=EntNt_AND(0,0,1,1,1,0);
    printf("0,0,1,1,1=%f\n",apr);
    apr=EntNt_AND(0,0,1,1,0,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=EntNt_AND(0,0,1,0,1,0);
    printf("0,0,1,0,1=%f\n",apr);
    apr=EntNt_AND(0,0,1,0,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=EntNt_AND(0,0,0,1,1,0);
    printf("0,0,0,1,1=%f\n",apr);
    apr=EntNt_AND(0,0,0,1,0,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=EntNt_AND(0,0,0,0,1,0);
    printf("0,0,0,0,1=%f\n",apr);
    apr=EntNt_AND(0,0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);

    printf("\n");
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", Pesos_AND[0]);
    printf("Peso 2 = %f\n", Pesos_AND[1]);
    printf("Peso 3 = %f\n", Pesos_AND[2]);
    printf("Peso 4 = %f\n", Pesos_AND[3]);
    printf("Peso 5 = %f\n", Pesos_AND[4]);
    printf("Bias");
    printf("Bias = %f",bias_AND);
    printf("Resultados\n");
    apr=InitNt_AND(1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=InitNt_AND(1,1,1,1,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=InitNt_AND(1,1,1,0,1);
    printf("1,1,1,0,1=%f\n",apr);
    apr=InitNt_AND(1,1,1,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=InitNt_AND(1,1,0,1,1);
    printf("1,1,0,1,1=%f\n",apr);
    apr=InitNt_AND(1,1,0,1,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=InitNt_AND(1,1,0,0,1);
    printf("1,1,0,0,1=%f\n",apr);
    apr=InitNt_AND(1,1,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=InitNt_AND(1,0,1,1,1);
    printf("1,0,1,1,1=%f\n",apr);
    apr=InitNt_AND(1,0,1,1,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=InitNt_AND(1,0,1,0,1);
    printf("1,0,1,0,1=%f\n",apr);
    apr=InitNt_AND(1,0,1,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=InitNt_AND(1,0,0,1,1);
    printf("1,0,0,1,1=%f\n",apr);
    apr=InitNt_AND(1,0,0,1,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=InitNt_AND(1,0,0,0,1);
    printf("1,0,0,0,1=%f\n",apr);
    apr=InitNt_AND(1,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=InitNt_AND(0,1,1,1,1);
    printf("0,1,1,1,1=%f\n",apr);
    apr=InitNt_AND(0,1,1,1,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=InitNt_AND(0,1,1,0,1);
    printf("0,1,1,0,1=%f\n",apr);
    apr=InitNt_AND(0,1,1,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=InitNt_AND(0,1,0,1,1);
    printf("0,1,0,1,1=%f\n",apr);
    apr=InitNt_AND(0,1,0,1,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=InitNt_AND(0,1,0,0,1);
    printf("0,1,0,0,1=%f\n",apr);
    apr=InitNt_AND(0,1,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=InitNt_AND(0,0,1,1,1);
    printf("0,0,1,1,1=%f\n",apr);
    apr=InitNt_AND(0,0,1,1,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=InitNt_AND(0,0,1,0,1);
    printf("0,0,1,0,1=%f\n",apr);
    apr=InitNt_AND(0,0,1,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=InitNt_AND(0,0,0,1,1);
    printf("0,0,0,1,1=%f\n",apr);
    apr=InitNt_AND(0,0,0,1,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=InitNt_AND(0,0,0,0,1);
    printf("0,0,0,0,1=%f\n",apr);
    apr=InitNt_AND(0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);
    //-----------------------------------------------------------------------

	printf("Salida Entrenamiento XOR:\n");
    apr=EntNt_XOR(1,1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=EntNt_XOR(1,1,1,1,0,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=EntNt_XOR(1,1,1,0,1,0);
    printf("1,1,1,0,1=%f\n",apr);
    apr=EntNt_XOR(1,1,1,0,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=EntNt_XOR(1,1,0,1,1,0);
    printf("1,1,0,1,1=%f\n",apr);
    apr=EntNt_XOR(1,1,0,1,0,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=EntNt_XOR(1,1,0,0,1,0);
    printf("1,1,0,0,1=%f\n",apr);
    apr=EntNt_XOR(1,1,0,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=EntNt_XOR(1,0,1,1,1,0);
    printf("1,0,1,1,1=%f\n",apr);
    apr=EntNt_XOR(1,0,1,1,0,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=EntNt_XOR(1,0,1,0,1,0);
    printf("1,0,1,0,1=%f\n",apr);
    apr=EntNt_XOR(1,0,1,0,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=EntNt_XOR(1,0,0,1,1,0);
    printf("1,0,0,1,1=%f\n",apr);
    apr=EntNt_XOR(1,0,0,1,0,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=EntNt_XOR(1,0,0,0,1,0);
    printf("1,0,0,0,1=%f\n",apr);
    apr=EntNt_XOR(1,0,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=EntNt_XOR(0,1,1,1,1,0);
    printf("0,1,1,1,1=%f\n",apr);
    apr=EntNt_XOR(0,1,1,1,0,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=EntNt_XOR(0,1,1,0,1,0);
    printf("0,1,1,0,1=%f\n",apr);
    apr=EntNt_XOR(0,1,1,0,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=EntNt_XOR(0,1,0,1,1,0);
    printf("0,1,0,1,1=%f\n",apr);
    apr=EntNt_XOR(0,1,0,1,0,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=EntNt_XOR(0,1,0,0,1,0);
    printf("0,1,0,0,1=%f\n",apr);
    apr=EntNt_XOR(0,1,0,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=EntNt_XOR(0,0,1,1,1,0);
    printf("0,0,1,1,1=%f\n",apr);
    apr=EntNt_XOR(0,0,1,1,0,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=EntNt_XOR(0,0,1,0,1,0);
    printf("0,0,1,0,1=%f\n",apr);
    apr=EntNt_XOR(0,0,1,0,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=EntNt_XOR(0,0,0,1,1,0);
    printf("0,0,0,1,1=%f\n",apr);
    apr=EntNt_XOR(0,0,0,1,0,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=EntNt_XOR(0,0,0,0,1,0);
    printf("0,0,0,0,1=%f\n",apr);
    apr=EntNt_XOR(0,0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);

    printf("\n");
    printf("Pesos de cada epoca\n");
    printf("Peso 1 = %f\n", Pesos_XOR[0]);
    printf("Peso 2 = %f\n", Pesos_XOR[1]);
    printf("Peso 3 = %f\n", Pesos_XOR[2]);
    printf("Peso 4 = %f\n", Pesos_XOR[3]);
    printf("Peso 5 = %f\n", Pesos_XOR[4]);
    printf("Bias");
    printf("Bias = %f",bias_XOR);
    printf("Resultados\n");
    apr=InitNt_XOR(1,1,1,1,1);
    printf("1,1,1,1,1=%f\n",apr);
    apr=InitNt_XOR(1,1,1,1,0);
    printf("1,1,1,1,0=%f\n",apr);
    apr=InitNt_XOR(1,1,1,0,1);
    printf("1,1,1,0,1=%f\n",apr);
    apr=InitNt_XOR(1,1,1,0,0);
    printf("1,1,1,0,0=%f\n",apr);
    apr=InitNt_XOR(1,1,0,1,1);
    printf("1,1,0,1,1=%f\n",apr);
    apr=InitNt_XOR(1,1,0,1,0);
    printf("1,1,0,1,0=%f\n",apr);
    apr=InitNt_XOR(1,1,0,0,1);
    printf("1,1,0,0,1=%f\n",apr);
    apr=InitNt_XOR(1,1,0,0,0);
    printf("1,1,0,0,0=%f\n",apr);
    apr=InitNt_XOR(1,0,1,1,1);
    printf("1,0,1,1,1=%f\n",apr);
    apr=InitNt_XOR(1,0,1,1,0);
    printf("1,0,1,1,0=%f\n",apr);
    apr=InitNt_XOR(1,0,1,0,1);
    printf("1,0,1,0,1=%f\n",apr);
    apr=InitNt_XOR(1,0,1,0,0);
    printf("1,0,1,0,0=%f\n",apr);
    apr=InitNt_XOR(1,0,0,1,1);
    printf("1,0,0,1,1=%f\n",apr);
    apr=InitNt_XOR(1,0,0,1,0);
    printf("1,0,0,1,0=%f\n",apr);
    apr=InitNt_XOR(1,0,0,0,1);
    printf("1,0,0,0,1=%f\n",apr);
    apr=InitNt_XOR(1,0,0,0,0);
    printf("1,0,0,0,0=%f\n",apr);
    apr=InitNt_XOR(0,1,1,1,1);
    printf("0,1,1,1,1=%f\n",apr);
    apr=InitNt_XOR(0,1,1,1,0);
    printf("0,1,1,1,0=%f\n",apr);
    apr=InitNt_XOR(0,1,1,0,1);
    printf("0,1,1,0,1=%f\n",apr);
    apr=InitNt_XOR(0,1,1,0,0);
    printf("0,1,1,0,0=%f\n",apr);
    apr=InitNt_XOR(0,1,0,1,1);
    printf("0,1,0,1,1=%f\n",apr);
    apr=InitNt_XOR(0,1,0,1,0);
    printf("0,1,0,1,0=%f\n",apr);
    apr=InitNt_XOR(0,1,0,0,1);
    printf("0,1,0,0,1=%f\n",apr);
    apr=InitNt_XOR(0,1,0,0,0);
    printf("0,1,0,0,0=%f\n",apr);
    apr=InitNt_XOR(0,0,1,1,1);
    printf("0,0,1,1,1=%f\n",apr);
    apr=InitNt_XOR(0,0,1,1,0);
    printf("0,0,1,1,0=%f\n",apr);
    apr=InitNt_XOR(0,0,1,0,1);
    printf("0,0,1,0,1=%f\n",apr);
    apr=InitNt_XOR(0,0,1,0,0);
    printf("0,0,1,0,0=%f\n",apr);
    apr=InitNt_XOR(0,0,0,1,1);
    printf("0,0,0,1,1=%f\n",apr);
    apr=InitNt_XOR(0,0,0,1,0);
    printf("0,0,0,1,0=%f\n",apr);
    apr=InitNt_XOR(0,0,0,0,1);
    printf("0,0,0,0,1=%f\n",apr);
    apr=InitNt_XOR(0,0,0,0,0);
    printf("0,0,0,0,0=%f\n",apr);
    //-----------------------------------------------------------------------

}
  return 0;
}
